﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    [Header("Game control settings")]
    public static int Round;                            // round number
    private int enemiesOnMap;                           // how many enemies are left on a map
    public static bool RoundFinished;                   // check if the round is finished (for Spawner)
    public static bool RoundIsOn;                       // check if the round is on
    public static bool gameOver;                        // check if game is over
    public Text gameOverText;

    [Header("Scene settings")]
    public Text cashText;
    public Text roundText;
    public AudioClip deathSound;
    private static AudioSource audioSource;
    private UIController ui;
    public GameObject target;

    [Header("Player settings")]
    public int startCash;
    public static int Cash;

    [Header("Turrets settings")]
    public int turretCost;
    public int turretDamageUpgradeCost;
    public int turretDamageUpgradeAmount;
    public int turretShootRateUpgradeCost;
    public float turretShootRateUpgradeAmount;

    public static int TurretInitialCost;
    public static int TurretDamageUpgradeCost;
    public static int TurretDamageUpgradeAmount;
    public static int TurretShootRateUpgradeCost;
    public static float TurretShootRateUpgradeAmount;

    [Header("AI settings")]
    public static string selectedAI;

    void Start()
    {
        ui = GetComponent<UIController>();

        audioSource = GetComponent<AudioSource>();
        audioSource.clip = deathSound;

        selectedAI = LevelManager.GetGameAI();

        Cash = startCash;

        TurretInitialCost = turretCost;
        TurretDamageUpgradeCost = turretDamageUpgradeCost;
        TurretDamageUpgradeAmount = turretDamageUpgradeAmount;
        TurretShootRateUpgradeCost = turretShootRateUpgradeCost;
        TurretShootRateUpgradeAmount = turretShootRateUpgradeAmount;

        Round = 1;

        ResetUponLoadScene();
    }


    void Update()
    {
        if (gameOver)
        {
            return;
        }
        else if (target == null && !gameOver)
        {
            End();
        }
        else if (!gameOver)
        {
            cashText.text = Cash + "$";

            RoundController();
        }
    }


    private void ResetUponLoadScene()
    {
        gameOver = false;
        RoundIsOn = false;
        RoundFinished = false;
    }


    private void RoundController()
    {
        // Scan level for Enemies objects while spawning or until there is no enemy left
        if (Spawner.Spawning || enemiesOnMap > 0)
        {
            enemiesOnMap = GameObject.FindGameObjectsWithTag("Enemy").Length;

            // Enemies in game = the round has started
            if (!RoundIsOn)
            {
                RoundIsOn = true;
                ui.Buttons_SetActive(false);
            }
        }

        // If the round is finished - all enemies are killed and no more will spawn
        if (RoundFinished && !Spawner.Spawning && enemiesOnMap == 0)
        {
            Round++;
            RoundFinished = false;
            roundText.text = "Round: " + Round;

            // No enemies in game = the round has finished
            RoundIsOn = false;
            ui.Buttons_SetActive(true);
        }
    }


    public static void ChargeCashForTurret()
    {
        Cash -= TurretInitialCost;
    }

    public static void ChargeCashForTurretDamageUpgrade()
    {
        Cash -= TurretDamageUpgradeCost;
    }

    public static void ChargeCashForTurretShootRateUpgrade()
    {
        Cash -= TurretShootRateUpgradeCost;
    }

    public static void PlayDeathSound()
    {
        audioSource.Play();
    }

    private void End()
    {
        gameOver = true;

        Time.timeScale = 0f;

        gameOverText.text = "GAME OVER";
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerTD : MonoBehaviour
{
    [Header("Scene settings")]
    public GameObject turretGameobject;
    public LayerMask clickableLayer;

    private Grid grid;

    [HideInInspector] public List<GameObject> clickedObjects;


    private void Start()
    {
        grid = GameObject.FindGameObjectWithTag("GameController").GetComponent<Grid>();
    }
    

    void Update()
    {
        if (GameController.RoundIsOn)
        {
            if (clickedObjects.Count > 0)
            {
                UnselectAll();
            }

            return;
        }

        if (Input.GetMouseButtonDown(0))                                      // on left mouse button click
        {
            Select();
        }
        else if (Input.GetMouseButtonDown(1))                                 // on right mouse button click
        {
            CreateTurret();
        }
    }


    /// <summary>
    /// Spawn an object at player's mouse position.
    /// </summary>
    private void CreateTurret()
    {
        // if player doesn't have enough money to place a turret
        if (GameController.Cash < 50)
        {
            return;
        }

        Collider2D[] collisions = Physics2D.OverlapCircleAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), 0.1f);
        bool placeableSpot = false;

        foreach (Collider2D collider in collisions)
        {
            // Check if player's object is already placed at mouse position
            if (collider.CompareTag("Gun"))
            {
                return;
            }
            // Check if pointed by mouse spot is placeable
            else if (collider.CompareTag("Placeable spot"))
            {
                placeableSpot = true;
            }
        }

        // If there is no player's object at mouse position, then place it
        if (placeableSpot)
        {
            if (!grid.NodeFromWorldPoint(Camera.main.ScreenToWorldPoint(Input.mousePosition)).walkable)   // bo inaczej da sie postawic na walkable odpowiednio klikajac myszka
            {
                Instantiate(turretGameobject, SnapToGrid(Camera.main.ScreenToWorldPoint(Input.mousePosition)), Quaternion.identity);    // spawn object
                GameController.ChargeCashForTurret();
            }
        }
    }

    
    /// <summary>
    /// Destroy selected turrets.
    /// </summary>
    public void DestroySelected()
    {
        foreach (GameObject obj in clickedObjects)
        {
            GameController.Cash += obj.GetComponent<Turret>().TurretCashValue() / 3;      // return some money for destroyed turret
            Destroy(obj);
        }
    }


    /// <summary>
    /// Snap GameObject to generated grid.
    /// </summary>
    /// <param name="mouseClickPosition">Position of click</param>
    /// <returns>Position in grid</returns>
    private Vector3 SnapToGrid(Vector3 mouseClickPosition)
    {
        return grid.NodeFromWorldPoint(mouseClickPosition).worldPosition;
    }


    /// <summary>
    /// Select turret.
    /// </summary>
    private void Select()
    {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, 0f, clickableLayer);

        if (hit)            // select object
        {
            if (!clickedObjects.Contains(hit.collider.gameObject))
            {
                clickedObjects.Add(hit.collider.gameObject);
                SetObjectColor(hit.collider.gameObject, true);
                Debug.Log(hit.collider.gameObject.name + " selected.");
            }
            else
            {
                clickedObjects.Remove(hit.collider.gameObject);
                SetObjectColor(hit.collider.gameObject, false);
                Debug.Log(hit.collider.gameObject.name + " unselected.");
            }
        }
    }


    /// <summary>
    /// Unselect all selected objects.
    /// </summary>
    public void UnselectAll()
    {
        foreach (GameObject obj in clickedObjects)
        {
            SetObjectColor(obj, false);
        }

        clickedObjects.Clear();
    }


    /// <summary>
    /// Set color of an object.
    /// </summary>
    /// <param name="obj">object to set color to</param>
    /// <param name="selected"></param>
    private void SetObjectColor(GameObject obj, bool selected)
    {
        SpriteRenderer sprite = obj.GetComponentInChildren<SpriteRenderer>();

        if (selected)       // set color of selected object
        {
            sprite.color = Color.red;
        }
        if (!selected)      // set color of unselected object
        {
            sprite.color = Color.white;
        }
    }


    public void UpgradeSelected_Damage()
    {
        // if player does not have enough money to upgrade selected turrets
        if (GameController.Cash < clickedObjects.Count * GameController.TurretDamageUpgradeCost)
        {
            return;
        }

        foreach (GameObject turret in clickedObjects)
        {
            turret.GetComponent<Turret>().UpgradeDamage();
        }
    }


    public void UpgradeSelected_ShootRate()
    {
        // if player does not have enough money to upgrade selected turrets
        if (GameController.Cash < clickedObjects.Count * GameController.TurretShootRateUpgradeCost)
        {
            return;
        }

        foreach (GameObject turret in clickedObjects)
        {
            turret.GetComponent<Turret>().UpgradeShootRate();
        }
    }
}
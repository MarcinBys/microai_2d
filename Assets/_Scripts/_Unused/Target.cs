﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    public float lifeTime;


    void Start()
    {
        DestroyOlderTargets();
    }


    void Update()
    {
        lifeTime -= Time.deltaTime;

        if (lifeTime <= 0)
        {
            Destroy(gameObject);
        }
    }


    /// <summary>
    /// Destroy existing target objects if they are named the same as this target.
    /// </summary>
    private void DestroyOlderTargets()
    {
        GameObject[] targets = GameObject.FindGameObjectsWithTag("Target");

        foreach (GameObject target in targets)
        {
            if (target.name == gameObject.name && target != gameObject)
            {
                Destroy(target);
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public List<GameObject> clickedObjects;

    public GameObject targetObject;

    private Grid grid;

    public LayerMask clickableLayer;


    private void Start()
    {
        grid = GameObject.FindGameObjectWithTag("GameController").GetComponent<Grid>();
    }
    

    void Update()
    {
        if (Input.GetMouseButtonDown(0))                                        // on left mouse button click
        {
            Select();
        }
        else if (Input.GetMouseButtonDown(1) && clickedObjects.Count != 0)      // on right mouse button click
        {
            Order();
        }
    }


    /// <summary>
    /// Select unit.
    /// </summary>
    private void Select()
    {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, 0f, clickableLayer);
        Debug.Log(hit);

        if (hit)            // select object
        {
            if (!clickedObjects.Contains(hit.collider.transform.parent.gameObject))
            {
                clickedObjects.Add(hit.collider.transform.parent.gameObject);
                SetObjectColor(hit.collider.transform.parent.gameObject, true);
            }

            Debug.Log(hit.collider.transform.parent.gameObject.name + " selected.");
        }
        else if (!hit)      // unselect all selected objects
        {
            foreach (GameObject obj in clickedObjects)
            {
                SetObjectColor(obj, false);
            }
                
            clickedObjects.Clear();
        }
    }


    /// <summary>
    /// Order selected units.
    /// </summary>
    private void Order()
    {
        if (!Physics2D.OverlapCircle(Camera.main.ScreenToWorldPoint(Input.mousePosition), 0.1f))            // if no collision
        {
            if (grid.NodeFromWorldPoint(Camera.main.ScreenToWorldPoint(Input.mousePosition)).walkable)      // if node is walkable
            {
                foreach (GameObject obj in clickedObjects)
                {
                    // Spawn target object
                    GameObject newTarget = Instantiate(targetObject, SnapToGrid(Camera.main.ScreenToWorldPoint(Input.mousePosition)), Quaternion.identity); // spawn target object
                    newTarget.name = obj.name + " (target)";                // set spawned target's name to "[Unit's name] (target)"

                    obj.GetComponent<Unit>().SetTarget(newTarget);          // set target to selected unit
                }
            }
        }
    }


    /// <summary>
    /// Set color of an object.
    /// </summary>
    /// <param name="obj">object to set color to</param>
    /// <param name="selected"></param>
    private void SetObjectColor(GameObject obj, bool selected)
    {
        SpriteRenderer sprite = obj.GetComponent<SpriteRenderer>();

        if (selected)       // set color of selected object
        {
            sprite.color = Color.red;
        }
        if (!selected)      // set color of unselected object
        {
            sprite.color = Color.white;
        }
    }


    /// <summary>
    /// Snap GameObject to generated grid.
    /// </summary>
    /// <param name="mouseClickPosition">Position of click</param>
    /// <returns>Position in grid</returns>
    private Vector3 SnapToGrid(Vector3 mouseClickPosition)
    {
        return grid.NodeFromWorldPoint(mouseClickPosition).worldPosition;
    }
}
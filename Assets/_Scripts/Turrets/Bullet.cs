﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Transform target;

    [Header("Object settings")]
    public GameObject impactEffect;

    [Header("Bullet properties")]
    private float speed;
    private float damageAmount;

    private void Start()
    {
        speed = 10f;
    }

    public void SetTarget(Transform _target)
    {
        target = _target;
    }


    public void SetDamageAmount(float value)
    {
        damageAmount = value;
    }


    void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Chase();
    }

    private void Chase()
    {
        Vector3 direction = target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;

        if (direction.magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        }

        transform.Translate(direction.normalized * distanceThisFrame, Space.World);
    }

    private void HitTarget()
    {
        GameObject effect = Instantiate(impactEffect, new Vector3(transform.position.x, transform.position.y, -1f), Quaternion.Euler(-90f, 0f, 0f));
        Destroy(effect, 1f);

        Damage(target);

        Destroy(gameObject);
    }


    private void Damage(Transform enemy)
    {
        if (enemy.gameObject != null)
        {
            enemy.GetComponent<Unit>().TakeDamage(damageAmount);
        }
    }
}
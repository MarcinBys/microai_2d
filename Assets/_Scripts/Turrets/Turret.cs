﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Turret : MonoBehaviour
{
    [Header("Turret's properties")]
    public float range;
    public float startDamageAmount;
    public float maxDamageAmount;
    public float startShootRate;
    public float maxShootRate;
    private float shootCooldown;

    [Header("Object settings")]
    public GameObject turretSprite;
    public GameObject bulletPrefab;
    public Transform bulletSpawnPoint;
    public Image shootRateBar;
    public Image damageBar;

    // === internal variables ===
    private Transform target;
    private float rotationSpeed;
    private int turretCashValue;
    private float shootRate;
    private float damageAmount;
    private AudioSource audioSource;


    void Start()
    {
        audioSource = GetComponent<AudioSource>();

        // Set default values
        turretCashValue = GameController.TurretInitialCost;
        rotationSpeed = 10f;
        shootCooldown = 0f;
        shootRate = startShootRate;
        damageAmount = startDamageAmount;

        // Update percentage bars
        shootRateBar.fillAmount = 1 - ((shootRate - maxShootRate) / (startShootRate - maxShootRate));
        damageBar.fillAmount = damageAmount / maxDamageAmount;

        InvokeRepeating("UpdateTargets", 0f, 0.5f);
    }

    public int TurretCashValue()
    {
        return turretCashValue;
    }

    public void IncreaseDamage(int value)
    {
        damageAmount += value;

        // Update damage bar
        damageBar.fillAmount = damageAmount / maxDamageAmount;
    }

    public void IncreaseShootRate(float value)
    {
        shootRate -= value;

        // Update shoot rate bar
        shootRateBar.fillAmount = 1 - ((shootRate - maxShootRate) / (startShootRate - maxShootRate));
    }

    public void IncreaseTurretCashValue(int value)
    {
        turretCashValue += value;
    }

    void Update()
    {
        if (shootCooldown > 0)
        {
            shootCooldown -= Time.deltaTime;
        }

        if (target != null)
        {
            // Rotate towards an enemy
            LockOnRotation();

            // Shoot if "reloaded"
            if (shootCooldown <= 0f)
            {
                Shoot();
                shootCooldown = shootRate;
            }
        }
    }

    private void UpdateTargets()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;

        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector2.Distance(transform.position, enemy.transform.position);

            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        // if nearest enemy is found AND he is within the range
        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
        }
        else
        {
            target = null;
        }
    }

    /// <summary>
    /// Rotate towards nearest enemy
    /// </summary>
    private void LockOnRotation()
    {
        Vector3 direction = target.position - turretSprite.transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        turretSprite.transform.rotation = Quaternion.Slerp(turretSprite.transform.rotation, q, Time.deltaTime * rotationSpeed);
    }


    private void Shoot()
    {
        GameObject bulletObject = Instantiate(bulletPrefab, bulletSpawnPoint.position, bulletSpawnPoint.rotation);

        Bullet bullet = bulletObject.GetComponent<Bullet>();

        if (bullet != null)
        {
            bullet.SetDamageAmount(damageAmount);
            bullet.SetTarget(target);

            audioSource.Play();
        }
    }


    public void UpgradeShootRate()
    {
        if (shootRate > maxShootRate)
        {
            IncreaseShootRate(GameController.TurretShootRateUpgradeAmount);
            IncreaseTurretCashValue(GameController.TurretShootRateUpgradeCost / 3);

            GameController.ChargeCashForTurretShootRateUpgrade();
        }
    }


    public void UpgradeDamage()
    {
        if (damageAmount < maxDamageAmount)
        {
            IncreaseDamage(GameController.TurretDamageUpgradeAmount);
            IncreaseTurretCashValue(GameController.TurretDamageUpgradeCost / 3);

            GameController.ChargeCashForTurretDamageUpgrade();
        }
    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;

public class EnemyUsingAStar : Unit
{
    private Pathfinding pathfinder;                 // access to Pathfinding script
    private List<Node> unitPath;                    // calculated unit's path

    private Transform target;                       // target position

    private bool moving;

    private float moveStartTime;

    void Start()
    {
        SnapToGrid();

        pathfinder = GameObject.FindGameObjectWithTag("GameController").GetComponent<Pathfinding>();

        moving = false;

        health = startHealth;

        // find current level's target for an enemy to go to
        SetTarget(GameObject.FindGameObjectWithTag("Target"));

        moveStartTime = Time.time;
    }


    void FixedUpdate()
    {
        Profiler.BeginSample("MoveToTarget Call (Path)");
        MoveToTarget();
        Profiler.EndSample();
    }


    /// <summary>
    /// Move unit by his path.
    /// </summary>
    private void MoveToTarget()
    {
        if (moving && target == null)
        {
            Debug.Log(gameObject.name + ": target lost.");                              // if target is lost (or reached, so the target has been destroyed and there is none)
            SetTarget();
            moving = false;
            UpdatePath();
        }
        else if ((unitPath == null || unitPath.Count == 0) && moving)                   // if target is reached OR blocked
        {
            if (target != null && transform.position == target.position)                // target reached
            {
                Debug.Log(gameObject.name + ": target reached.");
                Destroy(target.gameObject);
                Debug.Log("Elapsed time: " + (Time.time - moveStartTime));
                return;
            }
            else                                                                        // target blocked
            {
                Debug.Log(gameObject.name + ": target blocked.");
                UpdatePath();
            }
        }
        else if (target != null)                                                        // if target not reached
        {
            if (unitPath == null || unitPath.Count == 0)                                // check new target's position
            {
                Debug.Log(gameObject.name + ": target found, moving.");
                UpdatePath();
                moving = true;
            }
            else if (moving)                                                            // move to target
            {
                // if unit has reached first (from unitPath list) node's position
                if (transform.position == unitPath[0].worldPosition && unitPath.Count != 0)
                {
                    UpdatePath();
                }

                // if unitPath is not empty
                if (unitPath != null && unitPath.Count > 0 && target != null)
                {
                    // keep updating drawn path if there is only one (!) node in unitPath list (it is needed to draw the line to target)
                    if (unitPath.Count == 1)
                    {
                        DrawPath();
                    }

                    // move to the first on the list node's position
                    transform.position = Vector2.MoveTowards(transform.position, unitPath[0].worldPosition, Time.deltaTime * movementSpeed);
                }
            }
        }
    }


    /// <summary>
    /// Update unit's path when the destination node's position is reached.
    /// </summary>
    private void UpdatePath()
    {
        if (target != null)
        {
            Profiler.BeginSample("FindPath Call");
            unitPath = pathfinder.FindPath(transform.position, target.position);
            Profiler.EndSample();
        }
        else
        {
            unitPath = null;
        }

        DrawPath();
    }


    /// <summary>
    /// Sets current target.
    /// </summary>
    /// <param name="newTarget">null is default for clearing current target</param>
    public override void SetTarget(GameObject newTarget = null)
    {
        if (newTarget == null)      // clear current target
        {
            target = null;
        }
        else                        // set new target
        {
            target = newTarget.transform;
        }
    }


    /// <summary>
    /// Draw unit's path using LineRenderer component.
    /// </summary>
    protected override void DrawPath()
    {
        if (unitPath == null)               // clear drawn path
        {
            lineRenderer.positionCount = 0;
        }
        else if (unitPath.Count > 1)        // draw full path to the target
        {
            lineRenderer.positionCount = unitPath.Count;
            lineRenderer.sortingOrder = 1;

            for (int i = 0; i < unitPath.Count; i++)
            {
                lineRenderer.SetPosition(i, unitPath[i].worldPosition);
            }
        }
        else if (unitPath.Count == 1)       // draw path between 2 last points (unit and the last node in unitPath list)
        {
            lineRenderer.positionCount = 2;
            lineRenderer.SetPosition(0, transform.position);
            lineRenderer.SetPosition(1, unitPath[0].worldPosition);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(LineRenderer))]
public abstract class Unit : MonoBehaviour
{
    protected LineRenderer lineRenderer;                // for drawing unit's path

    [Header("Unity settings")]
    public GameObject deathEffect;                      // death particles
    public Image healthBar;

    [Header("Unit parameters")]
    [Range(0, 10)] public float movementSpeed;
    public float startHealth;
    public int cashValue;

    protected float health;

    public abstract void SetTarget(GameObject newTarget = null);

    protected abstract void DrawPath();

    void Awake()
    {
        //SnapToGrid();                                   // snap unit to grid on start

        lineRenderer = GetComponent<LineRenderer>();
    }


    /// <summary>
    /// Snap GameObject to generated grid.
    /// </summary>
    protected void SnapToGrid()
    {
        Grid grid = GameObject.FindGameObjectWithTag("GameController").GetComponent<Grid>();
        transform.position = grid.NodeFromWorldPoint(transform.position).worldPosition;
    }


    /// <summary>
    /// Deal damage to the unit.
    /// </summary>
    /// <param name="value"></param>
    public void TakeDamage(float value)
    {
        health -= value;

        healthBar.fillAmount = health / startHealth;

        if (health <= 0)
        {
            DestroyUnit();
        }
    }


    /// <summary>
    /// Destroy unit upon death.
    /// </summary>
    private void DestroyUnit()
    {
        Debug.Log(gameObject.name + ": died.");

        GameObject effect = Instantiate(deathEffect, new Vector3(transform.position.x, transform.position.y, -1f), Quaternion.Euler(-90f, 0f, 0f));
        Destroy(effect, 1f);

        GameController.Cash += cashValue;

        GameController.PlayDeathSound();

        Destroy(gameObject);
        return;
    }


    #region Unused code
    protected Node GetCurrentPosition()
    {
        Grid grid = GameObject.FindGameObjectWithTag("GameController").GetComponent<Grid>();

        Node myPosition = grid.NodeFromWorldPoint(transform.position);

        return myPosition;
    }
    #endregion
}
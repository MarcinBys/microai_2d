﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    void Start()
    {
        if (CheckSceneName("Menu"))
        {
            if (PlayerPrefs.GetString("AI").Equals("A*"))
            {
                FindObjectOfType<Slider>().value = 0;
            }
            else if (PlayerPrefs.GetString("AI").Equals("NavMesh"))
            {
                FindObjectOfType<Slider>().value = 1;
            }
        }
    }

    /// <summary>
    /// Method used to check if name of loaded scene contains given string.
    /// </summary>
    /// <param name="scene"> Fragment of scene name. </param>
    /// <returns> Returns true if scene name contains given string. </returns>
    public bool CheckSceneName(string scene)
    {
        if (SceneManager.GetActiveScene().name.Contains(scene))
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Load scene
    /// </summary>
    /// <param name="sceneName"></param>
    public void LoadScene(string sceneName)
    {
        // if current scene is Menu
        if (CheckSceneName("Menu"))
        {
            SetGameAI();
        }

        Debug.Log("Loading scene: " + sceneName);
        SceneManager.LoadScene(sceneName);
    }

    public void QuitGame()
    {
        Debug.Log("Quit!");
        Application.Quit();
    }


    private void SetGameAI()
    {
        Slider aiSlider = FindObjectOfType<Slider>();

        if (aiSlider.value.Equals(0))
        {
            PlayerPrefs.SetString("AI", "A*");
            Debug.Log("A*");
        }
        else if (aiSlider.value.Equals(1))
        {
            PlayerPrefs.SetString("AI", "NavMesh");
            Debug.Log("NavMesh");
        }
    }

    public static string GetGameAI()
    {
        return PlayerPrefs.GetString("AI");
    }
}
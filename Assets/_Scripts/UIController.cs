﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [Header("UI buttons")]
    public Button[] buttons;

    public void Buttons_SetActive(bool status)
    {
        foreach (Button button in buttons)
        {
            button.interactable = status;
        }
    }
}

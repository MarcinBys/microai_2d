﻿using UnityEngine;

public class Node
{
    public bool walkable;
    public Vector3 worldPosition;

    // Pozycje na siatce, do obliczania dystansu miedzy wezlami oraz odnajdowania sasiadow
    public Vector2 gridPosition;

    public int gCost;
    public int hCost;
    public Node parent;


    /// <summary>
    /// Konstruktor klasy węzeł.
    /// </summary>
    /// <param name="_walkable">Czy wezel jest przechodni</param>
    /// <param name="_worldPosition">Pozycja wezla w swiecie</param>
    /// <param name="_gridX">Pozycja x na siatce</param>
    /// <param name="_gridY">Pozycja y na siatce</param>
    public Node(bool _walkable, Vector3 _worldPosition, Vector2 _gridPosition)
    {
        walkable = _walkable;
        worldPosition = _worldPosition;
        gridPosition = _gridPosition;
    }


    /// <summary>
    /// Calculate and return F = G + H.
    /// </summary>
    public int fCost
    {
        get
        {
            return gCost + hCost;
        }
    }


    /// <summary>
    /// Get GameObject staying on this node.
    /// </summary>
    public GameObject GetBlockingObject
    {
        get
        {
            try
            {
                return Physics2D.OverlapCircle(worldPosition, 0.25f).gameObject;
            }
            catch
            {
                return null;
            }
        }
    }
}
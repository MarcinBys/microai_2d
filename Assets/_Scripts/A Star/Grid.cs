﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    public LayerMask unwalkableLayer;    
    public Vector2 gridWorldSize;       // obszar we wspolrzednych swiata, ktory zostanie pokryty przez siatke
    public float nodeRadius;            // promien wezla, ile miejsca zajmuje kazdy pojedynczy wezel, uzywane tez do zageszczania siatki

    private Node[,] grid;
    private float nodeDiameter;         // srednica wezla

    private int gridSizeX;
    private int gridSizeY;

    public List<Node> path;             // lista wezlow sciezki do kolorowania
    

    private void Awake()
    {
        nodeDiameter = nodeRadius * 2;

        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);

        CreateGrid();
    }


    private void Update()
    {
        //UpdateGrid();
    }


    /// <summary>
    /// Generowanie siatki. 
    /// Tworzenie siatki zaczyna sie od lewego dolnego rogu az do prawego gornego rogu.
    /// </summary>
    private void CreateGrid()
    {
        grid = new Node[gridSizeX, gridSizeY];

        //lewy dolny rog siatki =    srodek siatki   -  (1,0,0) * szerokosc calej siatki / 2 - (0,1,0) * dlugosc calej siatki / 2
        Vector3 worldBottomLeft = transform.position - (Vector3.right * gridWorldSize.x / 2) - (Vector3.up * gridWorldSize.y / 2);

        for (int x = 0; x < gridSizeX; x++)             // wiersze
        {
            for (int y = 0; y < gridSizeY; y++)         // kolumny
            {
                // miejsce w swiecie  = lewy dolny róg  + (1,0,0) * (x * srednica wezla + promien wezla)  +  (0,1,0) * (y * srednica wezla + promien wezla)
                Vector3 worldPosition = worldBottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.up * (y * nodeDiameter + nodeRadius);

                // Przykladowe obliczanie worldPosition, dla x=1 y=1 r=0.5:
                //   (-15, -15, 0) + (1, 0, 0) * (1 * 1 + 0.5) + (0, 1, 0) * (1 * 1 + 0.5) = 
                // = (-15, -15, 0) + (1.5, 0, 0) + (0, 1.5, 0) = 
                // = (-13.5, -13.5, 0)

                // collision check
                bool walkable = !Physics2D.OverlapCircle(worldPosition, nodeRadius - 0.05f, unwalkableLayer);

                // save node
                grid[x, y] = new Node(walkable, worldPosition, new Vector2(x, y));
            }
        }
    }


    /// <summary>
    /// Aktualizowanie siatki o nowe przeszkody (np. przesuwajace sie jednostki)
    /// </summary>
    private void UpdateGrid()
    {
        for (int x = 0; x < gridSizeX; x++)             // wiersze
        {
            for (int y = 0; y < gridSizeY; y++)         // kolumny
            {
                // collision check
                bool walkable = !Physics2D.OverlapCircle(grid[x, y].worldPosition, nodeRadius - 0.05f, unwalkableLayer);
                
                // if existing node is marked as walkable, but it is not walkable at the moment (or vice versa) (for example collision with unit is detected)
                if (grid[x, y].walkable != walkable)
                {
                    // update node
                    grid[x, y].walkable = !grid[x, y].walkable;
                }
            }
        }
    }


    /// <summary>
    /// Check all neighbouring nodes.
    /// </summary>
    /// <param name="node">Middle node</param>
    /// <returns>List of neighbouring nodes</returns>
    public List<Node> GetNeighbours(Node node)
    {
        List<Node> neighbours = new List<Node>();

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                // skip middle node, that's where we are
                if (x == 0 && y == 0)
                {
                    continue;
                }

                int checkX = (int)node.gridPosition.x + x;
                int checkY = (int)node.gridPosition.y + y;

                if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY)
                {
                    neighbours.Add(grid[checkX, checkY]);
                }
            }
        }

        return neighbours;
    }


    /// <summary>
    /// Convert world position into grid coordinate
    /// </summary>
    /// <param name="worldPosition"></param>
    /// <returns></returns>    
    public Node NodeFromWorldPoint(Vector3 worldPosition)
    {
        float percentX = (worldPosition.x + gridWorldSize.x / 2) / gridWorldSize.x;
        float percentY = (worldPosition.y + gridWorldSize.y / 2) / gridWorldSize.y;

        percentX = Mathf.Clamp01(percentX);
        percentY = Mathf.Clamp01(percentY);

        int x = Mathf.RoundToInt((gridSizeX - 1) * percentX);
        int y = Mathf.RoundToInt((gridSizeY - 1) * percentY);

        return grid[x, y];
    }

    
    /// <summary>
    /// Draw visible grid in Unity Editor
    /// </summary>
    private void OnDrawGizmos()
    {
        // Rysuj obszar siatki
        Gizmos.DrawWireCube(transform.position, new Vector3(gridWorldSize.x, gridWorldSize.y, 0));

        if (grid != null)
        {
            foreach (Node node in grid)
            {
                if (node.walkable)
                {
                    Gizmos.color = Color.white;
                }
                else
                {
                    Gizmos.color = Color.red;
                }
                
                // Zaznacz na okreslony kolor wyznaczona z wezlow sciezke
                if (path != null)
                {
                    if (path.Contains(node))
                    {
                        Gizmos.color = Color.yellow;
                    }
                }

                // Rysuj wezel
                Gizmos.DrawCube(node.worldPosition, Vector3.one * (nodeDiameter - .1f));
            }
        }
    }
}

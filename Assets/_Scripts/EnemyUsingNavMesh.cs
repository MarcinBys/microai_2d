﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Profiling;

public class EnemyUsingNavMesh : Unit
{
    private NavMeshAgent agent;

    private float moveStartTime;

    void Start()
    {
        SnapToGrid();

        agent = GetComponent<NavMeshAgent>();
        agent.updateRotation = false;
        agent.updateUpAxis = false;

        health = startHealth;
        agent.speed = movementSpeed;

        // find current level's target for an enemy to go to
        SetTarget(GameObject.FindGameObjectWithTag("Target"));

        moveStartTime = Time.time;
    }


    void FixedUpdate()
    {
        if (!agent.isStopped)
        {
            MovementEvents();
        }
    }


    /// <summary>
    /// Move unit by his path.
    /// </summary>
    private void MovementEvents()
    {
        if ((Vector2)transform.position == (Vector2)agent.destination)                // target reached
        {
            Debug.Log(gameObject.name + ": target reached.");
            Destroy(GameObject.FindGameObjectWithTag("Target"));
            SetTarget();

            Debug.Log("Elapsed time: " + (Time.time - moveStartTime));
            return;
        }

        DrawPath();
    }


    /// <summary>
    /// Sets or clears current target.
    /// </summary>
    /// <param name="newTarget">null is default for clearing current target</param>
    public override void SetTarget(GameObject newTarget = null)
    {
        if (newTarget == null)      // clear current target
        {
            agent.ResetPath();
            agent.isStopped = true;
        }
        else                        // set new target
        {
            Profiler.BeginSample("NavMesh SetDestination Call");
            agent.SetDestination(newTarget.transform.position);
            agent.isStopped = false;
            Profiler.EndSample();
        }
    }

    
    /// <summary>
    /// Draw unit's path using LineRenderer component.
    /// </summary>
    protected override void DrawPath()
    {
        if (!agent.hasPath)                 // clear drawn path
        {
            lineRenderer.positionCount = 0;
        }
        else if (agent.hasPath)             // draw full path to the target
        {
            lineRenderer.positionCount = agent.path.corners.Length;
            lineRenderer.sortingOrder = 1;

            for (int i = 0; i < agent.path.corners.Length; i++)
            {
                lineRenderer.SetPosition(i, agent.path.corners[i]);
            }
        }
    }
}
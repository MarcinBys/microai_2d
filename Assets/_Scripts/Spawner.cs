﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [Header("Scene settings")]
    public GameObject[] objectsToSpawn;
    public Transform[] spawnPoints;

    [Header("Spawner settings")]
    public float spawnRate;             // how fast objects should be spawned
    private int spawnAmount;            // how many objects to spawn
    private int spawnedAmount;          // how many objects has already been spawned

    public static bool Spawning;

    private void Start()
    {
        Spawning = false;
    }

    public void StartSpawning()
    {
        spawnAmount = GameController.Round;
        Spawning = true;

        InvokeRepeating("Spawn", 1f, spawnRate);
    }

    /// <summary>
    /// Spawn an object at spawn position.
    /// </summary>
    private void Spawn()
    {
        if (spawnedAmount < spawnAmount)
        {
            int randomValue = Random.Range(0, spawnPoints.Length);

            // Spawn prefab with A* or NavMesh AI
            if (GameController.selectedAI == "A*")
            {
                Instantiate(objectsToSpawn[0], spawnPoints[randomValue].position, Quaternion.identity);
            }
            else if (GameController.selectedAI == "NavMesh")
            {
                Instantiate(objectsToSpawn[1], spawnPoints[randomValue].position, Quaternion.identity);
            }

            spawnedAmount++;
        }
        else
        {
            // stop spawning enemies
            CancelInvoke("Spawn");

            // clear variable for the next wave
            spawnedAmount = 0;
            Spawning = false;

            // let GameController know that the round is finished
            GameController.RoundFinished = true;
        }
    }
}
